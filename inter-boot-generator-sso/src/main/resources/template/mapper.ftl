package ${packages}.mapper;

import ${packages}.entity.${entityClassName};
import org.apache.ibatis.annotations.Mapper;
import tech.mhuang.ext.interchan.core.mapper.BaseMapper;
/**
* ${description}JDBC接口
* @author: ${author}
* @date: ${date}
*/
@Mapper
public interface I${entityClassName}Service extends BaseMapper<${entityClassName},String>{

}