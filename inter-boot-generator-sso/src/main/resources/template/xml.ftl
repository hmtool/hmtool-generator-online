<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packages}.mapper.${entityClassName}Mapper">
    <resultMap id="BaseResultMap" type="${packages}.entity.${entityClassName}">
        <#list columns as column>
            <#if column.pk>
                <id column="${column.sourceName}" jdbcType="${column.mybatisType?upper_case}" property="${column.name}"/>
            </#if>
        </#list>
        <#list columns as column>
            <#if !column.pk>
                <result column="${column.sourceName}" jdbcType="${column.mybatisType?upper_case}" property="${column.name}"/>
            </#if>
        </#list>
    </resultMap>
    <sql id="Base_Column_List">
        <#list columns as column>
            ${column.sourceName}<#if column_has_next>,</#if>
        </#list>
    </sql>
    <select id="queryAll" resultMap="BaseResultMap">
        select <include refid="Base_Column_List"/> from ${sourceName}
    </select>

    <select id="pageCount" resultType="java.lang.Integer" parameterType="tech.mhuang.ext.interchan.protocol.data.Page">
        SELECT count(1)
        FROM ${sourceName} t
    </select>
    <select id="page" resultMap="BaseResultMap" parameterType="tech.mhuang.ext.interchan.protocol.data.Page">
        select <include refid="Base_Column_List"/> from ${sourceName} limit <#noparse>#{start},#{rows}</#noparse>
    </select>
<#list columns as column>
    <#if column.pk>
        <select id="getById" resultMap="BaseResultMap" parameterType="java.lang.${column.type}">
            select
            <include refid="Base_Column_List"/>
            from ${sourceName}
            where
            ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>
        </select>

        <delete id="delete" parameterType="java.lang.${column.type}">
            delete from ${sourceName}
            where
            ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>
        </delete>
        <#break>
    </#if>
</#list>

    <insert id="insert" parameterType="${packages}.entity.${entityClassName}">
        insert into ${sourceName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#list columns as column>
                <if test="${column.name} != null">
                    ${column.sourceName},
                </if>
            </#list>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <#list columns as column>
                <if test="${column.name} != null">
                    <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>,
                </if>
            </#list>
        </trim>
    </insert>
    <update id="update" parameterType="${packages}.entity.${entityClassName}">
        update ${sourceName}
        <set>
            <#list columns as column>
                <#if !column.pk>
                    <if test="${column.name} != null">
                        ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>,
                    </if>
                </#if>
            </#list>
        </set>
        <where>
            <#list columns as column>
                <#if column.pk>
                    and ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>
                </#if>
            </#list>
        </where>
    </update>
    <update id="updateAll" parameterType="${packages}.entity.${entityClassName}">
        update ${sourceName}
        <set>
            <#list columns as column>
                <#if !column.pk>
                    ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>,
                </#if>
            </#list>
        </set>
        <where>
            <#list columns as column>
                <#if column.pk>
                    and ${column.sourceName} = <#noparse>#{</#noparse>${column.name}<#noparse>}</#noparse>
                </#if>
            </#list>
        </where>
    </update>
</mapper>