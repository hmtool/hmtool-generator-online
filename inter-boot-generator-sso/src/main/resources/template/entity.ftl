package ${packages}.entity.${entityClassName};

import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.ext.interchan.core.entity.BaseEntity;

import java.io.Serializable;
<#if hasBigDecimal>
import java.lang.BigDecimal;
</#if>
<#if hasDate>
import java.util.Date;
</#if>
/**
* ${description}实体类
* @author: ${author}
* @date:   ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ${entityClassName} extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    <#list columns as column>
    /**
    * ${column.comments}
    */
    private ${column.type} ${column.name};
    </#list>
}