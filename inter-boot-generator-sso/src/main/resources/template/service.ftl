package ${packages}.service;

import ${packages}.entity.${entityClassName};
import tech.mhuang.ext.interchan.core.service.BaseService;
/**
 * ${description}服务接口
 * @author: ${author}
 * @date: ${date}
 */
public interface I${entityClassName}Service extends BaseService<${entityClassName},String>{

}