package ${packages}.service.impl;

import ${packages}.entity.${entityClassName};
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import tech.mhuang.ext.interchan.core.service.impl.BaseServiceImpl;
import ${packages}.service.I${entityClassName}Service;
import ${packages}.${entityClassName}Mapper;

/**
* ${description}服务实现类
* @author: ${author}
* @date: ${date}
*/
@Service
public class ${entityClassName}ServiceImpl extends BaseServiceImpl<${entityClassName},String> implements I${entityClassName}Service{

    @Autowired
    private  ${entityClassName}Mapper  ${entityClassName?uncap_first}Mapper;
}