package ${packages}.dto.${entityClassName}DTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.ext.interchan.core.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
<#if hasBigDecimal>
import java.lang.BigDecimal;
</#if>
<#if hasDate>
import java.util.Date;
</#if>
/**
* ${description}实体类
* @author: ${author}
* @date:   ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ${entityClassName}DTO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    <#list columns as column>
    /**
    * ${column.comments}
    */
    @ApiModelProperty(value = "${column.comments}")
    private ${column.type} ${column.name};
    </#list>
}