package tech.mhuang.interchan.generator.common;

import lombok.Getter;

public class DataBaseConsts {

    public  enum DataBaseType{
        MYSQL("1"),SQLSERVER("2"),ORACLE("3");
        @Getter
        private final String type;
        DataBaseType(String type){
            this.type = type;
        }
    }
}
