package tech.mhuang.interchan.generator.service.impl;

import org.springframework.stereotype.Service;
import tech.mhuang.ext.interchan.core.service.impl.BaseServiceImpl;
import tech.mhuang.interchan.generator.entity.GbDataBase;
import tech.mhuang.interchan.generator.service.IGbDataBaseService;

@Service
public class GbDataBaseServiceImpl extends BaseServiceImpl<GbDataBase,String> implements IGbDataBaseService {
}
