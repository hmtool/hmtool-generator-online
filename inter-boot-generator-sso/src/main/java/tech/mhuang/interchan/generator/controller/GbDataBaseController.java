package tech.mhuang.interchan.generator.controller;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.core.file.FileUtil;
import tech.mhuang.core.id.BaseIdeable;
import tech.mhuang.core.util.ObjectUtil;
import tech.mhuang.ext.interchan.core.controller.BaseController;
import tech.mhuang.ext.interchan.protocol.Result;
import tech.mhuang.ext.interchan.protocol.data.PageDTO;
import tech.mhuang.ext.interchan.protocol.data.PageVO;
import tech.mhuang.ext.spring.properties.RelaxedPropertyResolver;
import tech.mhuang.interchan.generator.dto.ColumnDTO;
import tech.mhuang.interchan.generator.dto.GeneratorTableDTO;
import tech.mhuang.interchan.generator.dto.TableDTO;
import tech.mhuang.interchan.generator.entity.GbDataBase;
import tech.mhuang.interchan.generator.manager.DataBaseManager;
import tech.mhuang.interchan.generator.service.IGbDataBaseService;
import tech.mhuang.interchan.generator.util.GeneratorUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@RestController("/gb/database")
@RequestMapping("/gb/database")
public class GbDataBaseController extends BaseController {

    @Autowired
    private IGbDataBaseService gbDataBaseService;

    @Autowired
    private BaseIdeable<String> baseIdeable;
    @GetMapping("/queryAll")
    public Result queryAll(){
        return Result.ok(gbDataBaseService.queryAll());
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody GbDataBase gbDataBase){
        gbDataBase.setId(baseIdeable.generateId());
        gbDataBaseService.insert(gbDataBase);
        return Result.ok();
    }

    @PutMapping("/update")
    public Result update(@RequestBody GbDataBase gbDataBase){
        gbDataBaseService.update(gbDataBase);
        return Result.ok();
    }

    @DeleteMapping("/delete")
    public Result delete(String id){
        gbDataBaseService.delete(id);
        return Result.ok();
    }

    @GetMapping("/check")
    public Result check(String id) throws SQLException {
        GbDataBase dataBase = gbDataBaseService.getById(id);
        if(ObjectUtil.isNotEmpty(dataBase)){
            boolean check = DataBaseManager.validate(dataBase);
            if(check){
                return Result.ok("数据库链接成功,开始获取数据库所有表");
            }else{
                return Result.faild("数据库链接失败");
            }
        }
        return Result.faild("未找到数据库配置");
    }

    @GetMapping("/pageTableById")
    public Result pageTableById(String id,String tableName, PageDTO pageDTO) throws SQLException {
        GbDataBase dataBase = gbDataBaseService.getById(id);
        if(ObjectUtil.isNotEmpty(dataBase)){
            PageVO<TableDTO> pageVO = new PageVO<>();
            int totalSize = DataBaseManager.countTable(dataBase,tableName);
            pageVO.setTotalSize(totalSize);
            if(totalSize > 0){
                pageVO.setResult(DataBaseManager.pageTable(dataBase,tableName,pageDTO));
            }
            return Result.ok(pageVO);
        }
        return Result.faild("未找到数据库配置");
    }

    @GetMapping("/queryColumnsById")
    public Result queryColumnsById(String id,String tableName) throws SQLException{
        GbDataBase dataBase = gbDataBaseService.getById(id);
        if(ObjectUtil.isNotEmpty(dataBase)){
            List<ColumnDTO> rst = DataBaseManager.queryColumnsById(dataBase,tableName);
            return Result.ok(rst);
        }
        return Result.faild("未找到数据库配置");
    }

    @Autowired
    private Environment environment;

    @GetMapping("/generator")
    public Result generator(String id,String packages, String tables) throws SQLException, IOException, TemplateException {
        GbDataBase dataBase = gbDataBaseService.getById(id);
        if(ObjectUtil.isNotEmpty(dataBase)){
            List<GeneratorTableDTO> generatorDTOList = DataBaseManager.queryColumnsByTableNames(dataBase, packages ,tables);
            String idName = GeneratorUtil.generator(generatorDTOList,new RelaxedPropertyResolver(environment, "mhuang.freemarker."));
            return Result.ok(idName);
        }
        return Result.faild("代码生成失败...");
    }

    @GetMapping("/download")
    public ResponseEntity<byte[]> download(String id,String fileName) throws IOException {
        String path = environment.getProperty("mhuang.freemarker.save_generator_path");
        File file = new File(path + id + ".zip");
        HttpHeaders headers = new HttpHeaders();
        // 通知浏览器以attachment（下载方式）打开图片
        headers.setContentDispositionFormData("attachment", fileName+".zip");
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(FileUtil.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }
}