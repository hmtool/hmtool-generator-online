package tech.mhuang.interchan.generator.dto;

import lombok.Data;

@Data
public class GeneratorDTO {
    private String tableName;
    private String tableComment;
    private String columnKey;
    private String columnName;
    private String columnType;
    private String columnComment;
}
