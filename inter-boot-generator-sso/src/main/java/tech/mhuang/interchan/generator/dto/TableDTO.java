package tech.mhuang.interchan.generator.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TableDTO {

    private String tableName;
    private String engine;
    private String tableCollation;
    private String tableComment;
    private Date createTime;
}
