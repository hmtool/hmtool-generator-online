package tech.mhuang.interchan.generator.manager.mysql;

import tech.mhuang.core.dict.BasicDict;

public class MysqlConsts {

    final static BasicDict FIELD_JAVA_TYPE = new BasicDict();
    final static BasicDict FIELD_MYBATIS_TYPE = new BasicDict();
    static{
        //数字
        FIELD_JAVA_TYPE.set("tinyint","Integer")
                    .set("smallint","Integer")
                    .set("mediumint","Integer")
                    .set("int","Integer")
                    .set("integer","Integer")
                    //Long
                    .set("bigint","Long")
                    //Float
                    .set("float","Float")
                    //Double
                    .set("double","Double")
                    //BigDecimal
                    .set("decimal","BigDecimal")
                    .set("numeric","BigDecimal")
                    //boolean
                    .set("bit","Boolean")
                    //String
                    .set("char","String")
                    .set("varchar","String")
                    .set("tinytext","String")
                    .set("text","String")
                    .set("mediumtext","String")
                    .set("longtext","String")
                    .set("json","String")
                    //Date
                    .set("date","Date")
                    .set("datetime","Date")
                    .set("timestamp","Date");

        FIELD_MYBATIS_TYPE.set("json","varchar")
                    .set("text","varchar")
                    .set("int","integer")
                    .set("datetime","timestamp");
    }
}
