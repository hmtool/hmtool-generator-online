package tech.mhuang.interchan.generator.mapper;

import org.apache.ibatis.annotations.Mapper;
import tech.mhuang.ext.interchan.core.mapper.BaseMapper;
import tech.mhuang.interchan.generator.entity.GbDataBase;

@Mapper
public interface GbDataBaseMapper extends BaseMapper<GbDataBase,String> {
    
}
