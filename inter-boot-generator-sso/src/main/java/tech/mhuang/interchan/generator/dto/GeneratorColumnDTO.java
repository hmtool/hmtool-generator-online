package tech.mhuang.interchan.generator.dto;

import lombok.Data;

@Data
public class GeneratorColumnDTO {

    /**
     * 是否是主键
     */
    private boolean pk;

    /**
     * 名称
     */
    private String name;

    /**
     * 原列名
     */
    private String sourceName;

    /**
     * 类型
     */
    private String type;

    /**
     * 原数据类型
     */
    private String sourceType;

    /**
     * mybatis对应的类型
     */
    private String mybatisType;

    /**
     * 备注
     */
    private String comments;

}
