package tech.mhuang.interchan.generator.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.ext.interchan.core.entity.BaseEntity;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class GbDataBase extends BaseEntity implements Serializable {

    private String id;

    private String type;

    private String dbName;

    private String ip;

    private String username;

    private String password;

    private String port;

    private String extra;

    private String remark;
}
