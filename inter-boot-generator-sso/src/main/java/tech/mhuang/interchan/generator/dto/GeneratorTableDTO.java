package tech.mhuang.interchan.generator.dto;

import lombok.Data;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Data
public class GeneratorTableDTO {

    /**
     * 生成的包路径
     */
    private String packages;

    /**
     * 表注释
     */
    private String description;

    /**
     * 作者
     */
    private String author;

    /**
     * 生成的类名
     */
    private String entityClassName;

    /**
     * 原表名
     */
    private String sourceName;

    /**
     * 是否有bigDecimal字段
     */
    private boolean hasBigDecimal;

    /**
     * 是否有时间字段
     */
    private boolean hasDate;

    /**
     * 创建时间
     */
    private String date;

    private List<GeneratorColumnDTO> columns = new CopyOnWriteArrayList<>();
}