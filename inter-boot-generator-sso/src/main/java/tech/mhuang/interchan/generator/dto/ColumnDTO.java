package tech.mhuang.interchan.generator.dto;

import lombok.Data;

@Data
public class ColumnDTO {

    private String columnName;
    private String columnType;
    private String columnComment;
    private String columnKey;
    private String isNullable;
    private String collationName;
    private String extra;
}
