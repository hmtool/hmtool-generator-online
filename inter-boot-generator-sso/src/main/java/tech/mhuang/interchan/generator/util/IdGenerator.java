package tech.mhuang.interchan.generator.util;


import tech.mhuang.core.id.BaseIdeable;
import tech.mhuang.core.id.SnowflakeIdeable;

/**
 * @author: mhuang
 * @Date: 2019/7/23 15:50
 * @Description:
 */
public class IdGenerator {

    private static BaseIdeable<String> idWorker = new SnowflakeIdeable();

    public static String nextId() {
        return idWorker.generateId();
    }
}