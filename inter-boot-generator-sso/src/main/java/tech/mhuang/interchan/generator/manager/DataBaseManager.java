package tech.mhuang.interchan.generator.manager;

import lombok.extern.slf4j.Slf4j;
import tech.mhuang.ext.interchan.protocol.data.PageDTO;
import tech.mhuang.interchan.generator.common.DataBaseConsts;
import tech.mhuang.interchan.generator.dto.ColumnDTO;
import tech.mhuang.interchan.generator.dto.GeneratorTableDTO;
import tech.mhuang.interchan.generator.dto.TableDTO;
import tech.mhuang.interchan.generator.entity.GbDataBase;
import tech.mhuang.interchan.generator.manager.mysql.MysqlManager;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Slf4j
public class DataBaseManager {

    public static boolean validate(GbDataBase dataBase) throws SQLException {
        if(DataBaseConsts.DataBaseType.MYSQL.getType().equals(dataBase.getType())){
            return MysqlManager.validate(dataBase);
        }
        return false;
    }

    public static int countTable(GbDataBase dataBase,String tableName) throws SQLException {
        if(DataBaseConsts.DataBaseType.MYSQL.getType().equals(dataBase.getType())){
            return MysqlManager.countTable(dataBase,tableName);
        }
        return 0;
    }

    public static List<TableDTO> pageTable(GbDataBase dataBase,String tableName, PageDTO pageDTO) throws SQLException {
        if(DataBaseConsts.DataBaseType.MYSQL.getType().equals(dataBase.getType())){
            return MysqlManager.pageTable(dataBase,tableName,pageDTO);
        }
        return Collections.EMPTY_LIST;
    }

    public static List<ColumnDTO> queryColumnsById(GbDataBase dataBase, String tableName) throws SQLException{
        if(DataBaseConsts.DataBaseType.MYSQL.getType().equals(dataBase.getType())){
            return MysqlManager.queryColumnsById(dataBase,tableName);
        }
        return Collections.EMPTY_LIST;
    }

    public static List<GeneratorTableDTO> queryColumnsByTableNames(GbDataBase dataBase,String packages, String tables) throws SQLException {
        if(DataBaseConsts.DataBaseType.MYSQL.getType().equals(dataBase.getType())){
            return  MysqlManager.queryColumnsByTableNames(dataBase,packages,tables);
        }
        return Collections.EMPTY_LIST;
    }
}
