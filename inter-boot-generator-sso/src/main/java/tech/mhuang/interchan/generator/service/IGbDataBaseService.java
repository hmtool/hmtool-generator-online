package tech.mhuang.interchan.generator.service;

import tech.mhuang.ext.interchan.core.service.BaseService;
import tech.mhuang.interchan.generator.entity.GbDataBase;

public interface IGbDataBaseService extends BaseService<GbDataBase,String> {
}
